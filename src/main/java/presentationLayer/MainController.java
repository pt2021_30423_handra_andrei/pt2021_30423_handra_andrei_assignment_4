package presentationLayer;

import businessLayer.Client;
import businessLayer.DeliveryService;
import dataLayer.Serializator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;

public class MainController {

    DeliveryService deliveryService;
    Serializator serializator;

    @FXML
    private Button register;
    @FXML
    private Button logInClient;
    @FXML
    private Button logInAdmin;
    @FXML
    private Button logInEmployee;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label errorLabel;
    @FXML
    private Button clear;


    Stage stage = new Stage();

    public MainController(){
        deliveryService = new DeliveryService();
        serializator = new Serializator();
        deliveryService = serializator.deserializeAll();

    }
    @FXML
    public void initialize(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/employee.fxml"));
            Parent root = fxmlLoader.load();
            stage.setScene(new Scene(root, 600, 600));
            stage.setTitle("Employee page");
            EmployeeController employeeController = fxmlLoader.getController();
            deliveryService.addObserver(employeeController);

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    @FXML
    public void handleKeyReleased(){
        String user = username.getText();
        String pass = password.getText();
        boolean enableButtons = (user.isEmpty() || user.trim().isEmpty()) && (pass.isEmpty() || pass.trim().isEmpty());
        logInAdmin.setDisable(enableButtons);
        logInClient.setDisable(enableButtons);
        logInEmployee.setDisable(enableButtons);
    }

    @FXML
    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(register)){
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/register.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root,600,600));
                stage.setTitle("Sign up");
                stage.show();
                RegisterController registerController = fxmlLoader.getController();
                registerController.setDeliveryService(deliveryService);

            }catch (IOException exception){
                exception.printStackTrace();
            }
        }else if(e.getSource().equals(logInAdmin)){
            System.out.println(deliveryService.getClients().size() + " clients");
            System.out.println(deliveryService.getAllProducts().size() + " products");
            System.out.println(deliveryService.getOrderInfo().size() + " orders");
            System.out.println(deliveryService.getAllProducts().get(2).getTimesOrdered());
            if(password.getText().isEmpty()){
                errorLabel.setText("Please enter a valid password");
                errorLabel.setVisible(true);
            }else {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/admin.fxml"));
                    Parent root = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root, 1050, 1000));
                    stage.setTitle("Administrator");
                    stage.show();
                    AdminController adminController = fxmlLoader.getController();
                    adminController.setDeliveryService(deliveryService);
                    logInEmployee.setDisable(true);
                    logInClient.setDisable(true);
                    logInAdmin.setDisable(true);
                    username.clear();
                    password.clear();

                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }else if(e.getSource().equals(logInClient)){
            int usernameExist = 0;
            if(password.getText().isEmpty()) {
                errorLabel.setText("Please enter a password");
                errorLabel.setVisible(true);

            }else {
                for (Client client : deliveryService.getClients()) {
                    if (client.getUsername().equals(username.getText())) {
                        usernameExist = 1;
                        if (client.getPassword().equals(password.getText())) {
                            try {
                                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/client.fxml"));
                                Parent root = fxmlLoader.load();
                                Stage stage = new Stage();
                                stage.setScene(new Scene(root, 1200, 1000));
                                stage.setTitle("Client");
                                stage.show();
                                ClientController clientController = fxmlLoader.getController();
                                clientController.setDeliveryService(deliveryService);
                                clientController.setClient(client);
                                logInEmployee.setDisable(true);
                                logInClient.setDisable(true);
                                logInAdmin.setDisable(true);
                                username.clear();
                                password.clear();

                            } catch (IOException exception) {
                                exception.printStackTrace();
                            }
                        } else {
                            errorLabel.setText("The password doesn't match");
                            errorLabel.setVisible(true);
                            username.clear();
                            password.clear();
                        }
                    }
                }
                if (deliveryService.getClients().size() == 0) {
                    errorLabel.setText("There are no users");
                    errorLabel.setVisible(true);
                } else if (usernameExist == 0) {
                    errorLabel.setText("The username doesn't exist");
                    errorLabel.setVisible(true);
                    username.clear();
                    password.clear();
                }
            }
        }else if(e.getSource().equals(logInEmployee)){

            if(password.getText().isEmpty()){
                errorLabel.setText("Please enter a valid password");
                errorLabel.setVisible(true);
            }else {
                    stage.show();
                    logInEmployee.setDisable(true);
                    logInClient.setDisable(true);
                    logInAdmin.setDisable(true);
                    username.clear();
                    password.clear();
            }
        }else if(e.getSource().equals(clear)){
            errorLabel.setVisible(false);
            errorLabel.setText("");
            logInEmployee.setDisable(true);
            logInClient.setDisable(true);
            logInAdmin.setDisable(true);
            username.clear();
            password.clear();
        }
    }




}
