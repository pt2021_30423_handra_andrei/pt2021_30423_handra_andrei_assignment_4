package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;
import dataLayer.Serializator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.List;

public class AdminController {

    DeliveryService deliveryService;
    Serializator serializator;

    @FXML
    private Button reports;
    @FXML
    private Label timeInterval;
    @FXML
    private TextField minHour;
    @FXML
    private Label and;
    @FXML
    private TextField maxHour;
    @FXML
    private Button enterTimeInterval;
    @FXML
    private Label productsOrderedMoreThan;
    @FXML
    private TextField minProductsOrdered;
    @FXML
    private Label times;
    @FXML
    private Button enterMinProducts;
    @FXML
    private Label clientsMoreThan;
    @FXML
    private TextField minClientsOrdered;
    @FXML
    private Label valueMoreThan;
    @FXML
    private TextField minValue;
    @FXML
    private Button enterMinValue;
    @FXML
    private Label ordered;
    @FXML
    private TextField dayOrdered;
    @FXML
    private Button enterDay;
    @FXML
    private Button importInitialProducts;
    @FXML
    private TableView<MenuItem> productsTable;
    @FXML
    private Button exit;
    @FXML
    private Button importProducts;
    @FXML
    private Button manageProducts;
    @FXML
    private Button deleteProduct;
    @FXML
    private Button enterDeleteProduct;
    @FXML
    private Label errorLabel;
    @FXML
    private Button modifyProduct;
    @FXML
    private Button enterModifyProduct;
    @FXML
    private Label title;
    @FXML
    private Label rating;
    @FXML
    private Label calories;
    @FXML
    private Label proteins;
    @FXML
    private Label fat;
    @FXML
    private Label sodium;
    @FXML
    private Label price;
    @FXML
    private TextField modifyTitle;
    @FXML
    private TextField modifyRating;
    @FXML
    private TextField modifyCalories;
    @FXML
    private TextField modifyProteins;
    @FXML
    private TextField modifyFat;
    @FXML
    private TextField modifySodium;
    @FXML
    private TextField modifyPrice;
    @FXML
    private Button enterBaseProduct;
    @FXML
    private Button addBaseProduct;
    @FXML
    private Button addCompositeProduct;
    @FXML
    private Button enterCompositeProduct;
    @FXML
    private Label compositeTitle;
    @FXML
    private TextField addCompositeTitle;



    private int initializeTable = 0;

    private MenuItem toDelete = null;
    private MenuItem toModify = null;
    private CompositeProduct compositeProduct;

    public void setDeliveryService(DeliveryService deliveryService){
        this.deliveryService = deliveryService;
    }


    @FXML
    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(reports)){
            timeInterval.setVisible(true);
            minHour.setVisible(true);
            and.setVisible(true);
            maxHour.setVisible(true);
            enterMinProducts.setVisible(true);
            enterTimeInterval.setVisible(true);
            productsOrderedMoreThan.setVisible(true);
            minProductsOrdered.setVisible(true);
            times.setVisible(true);
            clientsMoreThan.setVisible(true);
            minClientsOrdered.setVisible(true);
            valueMoreThan.setVisible(true);
            minValue.setVisible(true);
            enterMinValue.setVisible(true);
            ordered.setVisible(true);
            dayOrdered.setVisible(true);
            enterDay.setVisible(true);
        }else if(e.getSource().equals(importInitialProducts)){
            deliveryService.readFromCsv();
            if(initializeTable == 0) {
                productsTable = deliveryService.setTableHeaders(productsTable);
                initializeTable = 1;
            }
            for(MenuItem menuItem:deliveryService.getAllProducts()){
                productsTable.getItems().add(menuItem);
            }
            productsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("title"));
            productsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("rating"));
            productsTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("calories"));
            productsTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("protein"));
            productsTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("fat"));
            productsTable.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("sodium"));
            productsTable.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("price"));
        }else if(e.getSource().equals(exit)){
            serializator = new Serializator();
            serializator.serializeAll(deliveryService);
            Stage stage = (Stage) exit.getScene().getWindow();
            stage.close();
        }else if(e.getSource().equals(importProducts)){

            if(initializeTable == 0) {
                productsTable = deliveryService.importProducts(productsTable, initializeTable,deliveryService.getAllProducts());
                initializeTable = 1;
            }else{
                productsTable = deliveryService.importProducts(productsTable, initializeTable,deliveryService.getAllProducts());
            }
            manageProducts.setDisable(false);
        }else if(e.getSource().equals(manageProducts)){
            deleteProduct.setDisable(true);
            deleteProduct.setVisible(true);
            enterDeleteProduct.setVisible(true);

            modifyProduct.setDisable(true);
            modifyProduct.setVisible(true);
            enterModifyProduct.setVisible(true);

            enterBaseProduct.setVisible(true);
            addBaseProduct.setDisable(true);
            addBaseProduct.setVisible(true);

            enterCompositeProduct.setVisible(true);
            addCompositeProduct.setVisible(true);
            compositeProduct = new CompositeProduct();
            addCompositeTitle.setVisible(true);
            compositeTitle.setVisible(true);

            title.setVisible(true);
            rating.setVisible(true);
            calories.setVisible(true);
            proteins.setVisible(true);
            fat.setVisible(true);
            sodium.setVisible(true);
            price.setVisible(true);
            modifyTitle.setVisible(true);
            modifyRating.setVisible(true);
            modifyCalories.setVisible(true);
            modifyProteins.setVisible(true);
            modifyFat.setVisible(true);
            modifySodium.setVisible(true);
            modifyPrice.setVisible(true);


        }else if(e.getSource().equals(enterDeleteProduct)){
            toDelete = productsTable.getSelectionModel().getSelectedItem();
            deleteProduct.setDisable(false);
        }else if(e.getSource().equals(deleteProduct)){
            List<MenuItem> tempList;
            tempList = deliveryService.deleteProduct(toDelete,deliveryService.getAllProducts());
            deliveryService.setAllProducts(tempList);
            errorLabel.setText("The item was deleted. Press exit");
            errorLabel.setVisible(true);

        }else if(e.getSource().equals(enterModifyProduct)){
            toModify = productsTable.getSelectionModel().getSelectedItem();
            modifyProduct.setDisable(false);

        }else if(e.getSource().equals(modifyProduct)){
            MenuItem menuItem = new MenuItem();
            if(!modifyTitle.getText().isEmpty()){
                menuItem.setTitle(modifyTitle.getText());
            }else{
                menuItem.setTitle(toModify.getTitle());
            }
            if(!modifyRating.getText().isEmpty()){
                menuItem.setRating(Double.parseDouble(modifyRating.getText()));
            }else{
                menuItem.setRating(toModify.getRating());
            }
            if(!modifyCalories.getText().isEmpty()){
                menuItem.setCalories(Integer.parseInt(modifyCalories.getText()));
            }else{
                menuItem.setCalories(toModify.getCalories());
            }
            if(!modifyProteins.getText().isEmpty()){
                menuItem.setProtein(Integer.parseInt(modifyProteins.getText()));
            }else{
                menuItem.setProtein(toModify.getProtein());
            }
            if(!modifyFat.getText().isEmpty()){
                menuItem.setFat(Integer.parseInt(modifyFat.getText()));
            }else{
                menuItem.setFat(toModify.getFat());
            }
            if(!modifySodium.getText().isEmpty()){
                menuItem.setSodium(Integer.parseInt(modifySodium.getText()));
            }else{
                menuItem.setSodium(toModify.getSodium());
            }
            if(!modifyPrice.getText().isEmpty()){
                menuItem.computePrice(Integer.parseInt(modifyPrice.getText()));
            }else{
                menuItem.computePrice(toModify.getPrice());
            }
            List<MenuItem> tempList;
            tempList = deliveryService.modifyProduct(menuItem,toModify,deliveryService.getAllProducts());
            deliveryService.setAllProducts(tempList);

            errorLabel.setText("The item was modified. Press exit");
            errorLabel.setVisible(true);

        }else if(e.getSource().equals(enterBaseProduct)){
            if(!modifyTitle.getText().isEmpty() && !modifyRating.getText().isEmpty() && !modifyCalories.getText().isEmpty() && !modifyProteins.getText().isEmpty()
            && !modifyFat.getText().isEmpty() && !modifySodium.getText().isEmpty() && !modifyPrice.getText().isEmpty() ){
                addBaseProduct.setDisable(false);
            } else{
                errorLabel.setText("Please complete all the fields");
                errorLabel.setVisible(true);
            }

        }else if(e.getSource().equals(addBaseProduct)){
            BaseProduct baseProduct = new BaseProduct();
            boolean unique = true;
            baseProduct.setTitle(modifyTitle.getText());
            baseProduct.setRating(Double.parseDouble(modifyRating.getText()));
            baseProduct.setCalories(Integer.parseInt(modifyCalories.getText()));
            baseProduct.setProtein(Integer.parseInt(modifyProteins.getText()));
            baseProduct.setFat(Integer.parseInt(modifyFat.getText()));
            baseProduct.setSodium(Integer.parseInt(modifySodium.getText()));
            baseProduct.computePrice(Integer.parseInt(modifyPrice.getText()));
            for(MenuItem menuItem:deliveryService.getAllProducts()){
                if(menuItem.getTitle().equals(baseProduct.getTitle())){
                    unique = false;
                }
            }
            if(!unique){
                errorLabel.setText("The item already exists");
                errorLabel.setVisible(true);
            }else {
                List<MenuItem> tempList;
                tempList = deliveryService.addBaseProduct(baseProduct,deliveryService.getAllProducts());
                deliveryService.setAllProducts(tempList);
                errorLabel.setText("The item was added. Press exit");
                errorLabel.setVisible(true);
            }
        }else if(e.getSource().equals(enterCompositeProduct)){
            compositeProduct.getCompositeProducts().add(productsTable.getSelectionModel().getSelectedItem());
        }else if(e.getSource().equals(addCompositeProduct)){
            compositeProduct.setTitle(addCompositeTitle.getText());
            Double allRating = 0.0;
            Double newRating = 0.0;
            int newCalories = 0;
            int newProteins = 0;
            int newFat = 0;
            int newSodium = 0;
            int newPrice = 0;
            for(MenuItem menuItem: compositeProduct.getCompositeProducts()){
                allRating += menuItem.getRating();
                newCalories += menuItem.getCalories();
                newProteins += menuItem.getProtein();
                newFat += menuItem.getFat();
                newSodium += menuItem.getSodium();
                newPrice += menuItem.getPrice();
            }
            newRating = allRating/compositeProduct.getCompositeProducts().size();
            compositeProduct.setRating(newRating);
            compositeProduct.setCalories(newCalories);
            compositeProduct.setProtein(newProteins);
            compositeProduct.setFat(newFat);
            compositeProduct.setSodium(newSodium);
            compositeProduct.computePrice();
            boolean unique = true;
            for(MenuItem menuItem:deliveryService.getAllProducts()){
                if(menuItem.getTitle().equals(compositeProduct.getTitle())){
                    unique = false;
                }
            }
            if(!unique){
                errorLabel.setText("The item already exists");
                errorLabel.setVisible(true);
            }else {
                List<MenuItem> tempList;
                tempList = deliveryService.addCompositeProduct(compositeProduct,deliveryService.getAllProducts());
                deliveryService.setAllProducts(tempList);
                errorLabel.setText("The item was added. Press exit");
                errorLabel.setVisible(true);
            }
        }else if(e.getSource().equals(enterTimeInterval)){
            int min =Integer.parseInt(minHour.getText());
            int max = Integer.parseInt(maxHour.getText());
            int size = deliveryService.generateMinMaxHours(min,max,deliveryService.getOrderInfo());
            if(size == 0){
                errorLabel.setText("There are no orders between these hours");
                errorLabel.setVisible(true);
            }
        }else if(e.getSource().equals(enterMinProducts)){
            int nrOfTimes =Integer.parseInt(minProductsOrdered.getText());
            deliveryService.generateMinProducts(nrOfTimes,deliveryService.getOrderInfo());
        }else if(e.getSource().equals(enterMinValue)){
            int valueOfOrder = Integer.parseInt(minValue.getText());
            int nrTimesClientOrdered = Integer.parseInt(minClientsOrdered.getText());
            int size = deliveryService.generateReportWithClients(valueOfOrder,nrTimesClientOrdered,deliveryService.getClients(),deliveryService.getOrderInfo());
            if(size == 0){
                errorLabel.setText("There are no orders in this day");
                errorLabel.setVisible(true);
            }
        }else if(e.getSource().equals(enterDay)){
            int day = Integer.parseInt(dayOrdered.getText());
            int size = deliveryService.generateOrderedInDay(day,deliveryService.getOrderInfo());
            if(size == 0){
                errorLabel.setText("There are no orders in this day");
                errorLabel.setVisible(true);
            }
        }
    }
}
