package presentationLayer;

import businessLayer.Client;
import businessLayer.DeliveryService;
import dataLayer.Serializator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.util.Random;

public class RegisterController {

    DeliveryService deliveryService;
    Serializator serializator;

    @FXML
    private Button signUp;
    @FXML
    private Label errorLabel;
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private TextField age;
    @FXML
    private TextField email;

    public void setDeliveryService(DeliveryService deliveryService){
        this.deliveryService= deliveryService;
    }


    @FXML
    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(signUp)){
            if(username.getText().isEmpty() || password.getText().isEmpty() || age.getText().isEmpty() || email.getText().isEmpty()){
                errorLabel.setText("Please complete all the fields");
            }else{
                Client client= new Client();
                serializator = new Serializator();
                Random random = new Random();
                int id = random.nextInt(9000000-1)+1;
                loop:
                for(Client clients: deliveryService.getClients()){
                    if(clients.getId() == id){
                        id = random.nextInt(9000000-1)+1;
                        continue loop;
                    }
                }
                client.setId(id);
                client.setUsername(username.getText());
                client.setPassword(password.getText());
                client.setAge(Integer.parseInt(age.getText()));
                client.setEmail(email.getText());
                client.setTimesOrdered(0);
                deliveryService.getClients().add(client);
                serializator.serializeAll(deliveryService);
                Stage stage = (Stage) signUp.getScene().getWindow();
                stage.close();
            }
        }
    }
}
