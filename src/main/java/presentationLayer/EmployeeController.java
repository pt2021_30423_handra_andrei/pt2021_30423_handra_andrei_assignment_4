package presentationLayer;

import businessLayer.DeliveryService;
import businessLayer.Order;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.Observable;
import java.util.Observer;

public class EmployeeController implements Observer {

    @FXML
    private Label notifyEmployee;

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Order){
            Order order = (Order)arg;
            String string = "The order with " + order.getOrderId() + " has been made.";
            System.out.println(string);
            notifyEmployee.setText(string);
        }
    }
}
