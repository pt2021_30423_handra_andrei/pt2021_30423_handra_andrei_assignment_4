package presentationLayer;

import businessLayer.Client;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;
import businessLayer.Order;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dataLayer.FileWriter;
import dataLayer.Serializator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

public class ClientController {

    DeliveryService deliveryService;
    Client client;

    @FXML
    private Button makeOrder;
    @FXML
    private Button addProduct;
    @FXML
    private Button placeOrder;
    @FXML
    private Button viewMenu;
    @FXML
    private TableView<MenuItem> menuTable;
    @FXML
    private Button search;
    @FXML
    private Button filtersSelect;
    @FXML
    private CheckBox keyword;
    @FXML
    private CheckBox rating;
    @FXML
    private CheckBox calories;
    @FXML
    private CheckBox proteins;
    @FXML
    private CheckBox fats;
    @FXML
    private CheckBox sodium;
    @FXML
    private CheckBox price;
    @FXML
    private Label keywordLabel;
    @FXML
    private TextField searchKeyword;
    @FXML
    private Label ratingLabel;
    @FXML
    private TextField searchRating;
    @FXML
    private Label caloriesLabel;
    @FXML
    private TextField searchCalories;
    @FXML
    private Label proteinLabel;
    @FXML
    private TextField searchProtein;
    @FXML
    private Label fatLabel;
    @FXML
    private TextField searchFat;
    @FXML
    private Label sodiumLabel;
    @FXML
    private TextField searchSodium;
    @FXML
    private Label priceLabel;
    @FXML
    private TextField searchPrice;
    @FXML
    private Button clearFilters;
    @FXML
    private Label errorLabel;

    private boolean keywordSelected = false;
    private boolean ratingSelected = false;
    private boolean caloriesSelected = false;
    private boolean proteinsSelected = false;
    private boolean fatSelected = false;
    private boolean sodiumSelected = false;
    private boolean priceSelected = false;

    private int prevKey = 0;
    private int prevRating = 0;
    private int prevCalories = 0;
    private int prevProteins = 0;
    private int prevSodium = 0;
    private int prevFat = 0;
    private int prevPrice = 0;

    private List<MenuItem> orderProducts;
    private Order order;


    private int initializeTable = 0;

    public void setDeliveryService(DeliveryService deliveryService){
        this.deliveryService = deliveryService;
    }

    public void setClient(Client client){
        this.client = client;
    }

    public void onButtonClicked(ActionEvent e){
        if(e.getSource().equals(makeOrder)){
            addProduct.setVisible(true);
            placeOrder.setVisible(true);
            orderProducts = new ArrayList<>();
            order = new Order();
        }else if(e.getSource().equals(viewMenu)){
            if(initializeTable == 0){
                menuTable = deliveryService.importProducts(menuTable,initializeTable,deliveryService.getAllProducts());
                initializeTable = 1;
            }else{
                menuTable = deliveryService.importProducts(menuTable,initializeTable,deliveryService.getAllProducts());
            }
        }else if(e.getSource().equals(search)){
            List<MenuItem> filterKey = null;
            List<MenuItem> filterRating = null;
            List<MenuItem> filterCalories = null;
            List<MenuItem> filterProteins = null;
            List<MenuItem> filterFat = null;
            List<MenuItem> filterSodium = null;
            List<MenuItem> filterPrice = null;
            menuTable.getItems().clear();
            String last = null;
            if(keywordSelected){
                String text = searchKeyword.getText();
                filterKey = deliveryService.searchByKeyword(deliveryService.getAllProducts(),text);
                prevKey = 1;
                last = "keyword";
            }
            if(ratingSelected && prevKey == 1){
                String text = searchRating.getText();
                filterRating = deliveryService.searchByRating(filterKey,text);
                prevRating = 1;
                last = "rating";
            }else if(ratingSelected && prevKey == 0){
                String text = searchRating.getText();
                filterRating = deliveryService.searchByRating(deliveryService.getAllProducts(),text);
                prevRating = 1;
                last = "rating";
            }
            if(caloriesSelected && prevRating == 0 && prevKey == 0){
                String text = searchCalories.getText();
                filterCalories = deliveryService.searchByCalories(deliveryService.getAllProducts(),text);
                prevCalories = 1;
                last = "calories";
            }else if(caloriesSelected && prevRating == 0 && prevKey == 1){
                String text = searchCalories.getText();
                filterCalories = deliveryService.searchByCalories(filterKey,text);
                prevCalories = 1;
                last = "calories";
            }else if(caloriesSelected && prevRating == 1){
                String text = searchCalories.getText();
                filterCalories = deliveryService.searchByCalories(filterRating,text);
                prevCalories = 1;
                last = "calories";
            }
            if(proteinsSelected && prevKey == 0 && prevRating == 0 && prevCalories == 0){
                String text = searchProtein.getText();
                filterProteins = deliveryService.searchByProteins(deliveryService.getAllProducts(),text);
                prevProteins = 1;
                last = "proteins";
            }else if(proteinsSelected && prevKey == 1 && prevRating == 0 && prevCalories == 0){
                String text = searchProtein.getText();
                filterProteins = deliveryService.searchByProteins(filterKey,text);
                prevProteins = 1;
                last = "proteins";
            }else if(proteinsSelected && prevRating == 1 && prevCalories == 0){
                String text = searchProtein.getText();
                filterProteins = deliveryService.searchByProteins(filterRating,text);
                prevProteins = 1;
                last = "proteins";
            }else if(proteinsSelected && prevCalories == 1){
                String text = searchProtein.getText();
                filterProteins = deliveryService.searchByProteins(filterCalories,text);
                prevProteins = 1;
                last = "proteins";
            }
            if(fatSelected && prevKey == 0 && prevRating == 0 && prevCalories == 0 && prevProteins == 0){
                String text = searchFat.getText();
                filterFat = deliveryService.searchByFat(deliveryService.getAllProducts(),text);
                prevFat = 1;
                last = "fat";
            }else if(fatSelected && prevKey == 1 && prevRating == 0 && prevCalories == 0 && prevProteins == 0){
                String text = searchFat.getText();
                filterFat = deliveryService.searchByFat(filterKey,text);
                prevFat = 1;
                last = "fat";
            }else if(fatSelected && prevRating == 1 && prevCalories == 0 && prevProteins == 0){
                String text = searchFat.getText();
                filterFat = deliveryService.searchByFat(filterRating,text);
                prevFat = 1;
                last = "fat";
            }else if(fatSelected && prevCalories == 1 && prevProteins == 0){
                String text = searchFat.getText();
                filterFat = deliveryService.searchByFat(filterCalories,text);
                prevFat = 1;
                last = "fat";
            }else if(fatSelected && prevProteins == 1){
                String text = searchFat.getText();
                filterFat = deliveryService.searchByFat(filterProteins,text);
                prevFat = 1;
                last = "fat";
            }
            if(sodiumSelected && prevKey == 0 && prevRating == 0 && prevCalories == 0 && prevProteins == 0 && prevFat == 0){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(deliveryService.getAllProducts(),text);
                prevSodium = 1;
                last = "sodium";
            }else if(sodiumSelected && prevKey == 1 && prevRating == 0 && prevCalories == 0 && prevProteins == 0 && prevFat == 0){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(filterKey,text);
                prevSodium = 1;
                last = "sodium";
            }else if(sodiumSelected && prevRating == 1 && prevCalories == 0 && prevProteins == 0 && prevFat == 0){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(filterRating,text);
                prevSodium = 1;
                last = "sodium";
            }else if(sodiumSelected && prevCalories == 1 && prevProteins == 0 && prevFat == 0){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(filterCalories,text);
                prevSodium = 1;
                last = "sodium";
            }else if(sodiumSelected && prevProteins == 1 && prevFat == 0){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(filterProteins,text);
                prevSodium = 1;
                last = "sodium";
            }else if(sodiumSelected && prevFat == 1){
                String text = searchSodium.getText();
                filterSodium = deliveryService.searchBySodium(filterFat,text);
                prevSodium = 1;
                last = "sodium";
            }
            if(priceSelected && prevKey == 0 && prevRating == 0 && prevCalories == 0 && prevProteins == 0 && prevFat == 0 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(deliveryService.getAllProducts(), text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevKey == 1 && prevRating == 0 && prevCalories == 0 && prevProteins == 0 && prevFat == 0 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterKey, text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevRating == 1 && prevCalories == 0 && prevProteins == 0 && prevFat == 0 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterRating, text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevCalories == 1 && prevProteins == 0 && prevFat == 0 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterCalories, text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevProteins == 1 && prevFat == 0 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterProteins, text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevFat == 1 && prevSodium == 0) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterFat, text);
                prevPrice = 1;
                last = "price";
            }else if(priceSelected && prevSodium == 1) {
                String text = searchPrice.getText();
                filterPrice = deliveryService.searchByPrice(filterSodium, text);
                prevPrice = 1;
                last = "price";
            }
            if(last.equals("keyword")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterKey);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterKey);
                }
            }else if(last.equals("rating")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterRating);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterRating);
                }
            }else if(last.equals("calories")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterCalories);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterCalories);
                }
            }else if(last.equals("proteins")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterProteins);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterProteins);
                }
            }else if(last.equals("fat")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterFat);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterFat);
                }
            }else if(last.equals("sodium")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterSodium);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterSodium);
                }
            }else if(last.equals("price")){
                if(initializeTable == 0){
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterPrice);
                    initializeTable = 1;
                }else{
                    menuTable = deliveryService.importProducts(menuTable,initializeTable,filterPrice);
                }
            }
        }else if(e.getSource().equals(filtersSelect)){
            if(keyword.isSelected()){
                keywordSelected = true;
                keywordLabel.setVisible(true);
                searchKeyword.setVisible(true);
            }
            if(rating.isSelected()){
                ratingSelected = true;
                ratingLabel.setVisible(true);
                searchRating.setVisible(true);
            }
            if(calories.isSelected()){
                caloriesSelected = true;
                caloriesLabel.setVisible(true);
                searchCalories.setVisible(true);
            }
            if(proteins.isSelected()){
                proteinsSelected = true;
                proteinLabel.setVisible(true);
                searchProtein.setVisible(true);
            }
            if(fats.isSelected()){
                fatSelected = true;
                fatLabel.setVisible(true);
                searchFat.setVisible(true);
            }
            if(sodium.isSelected()){
                sodiumSelected = true;
                sodiumLabel.setVisible(true);
                searchSodium.setVisible(true);
            }
            if(price.isSelected()){
                priceSelected = true;
                priceLabel.setVisible(true);
                searchPrice.setVisible(true);
            }
        }else if(e.getSource().equals(clearFilters)){
             prevKey = 0;
             prevRating = 0;
             prevCalories = 0;
             prevProteins = 0;
             prevSodium = 0;
             prevFat = 0;
             prevPrice = 0;
             keywordSelected = false;
             ratingSelected = false;
             caloriesSelected = false;
             proteinsSelected = false;
             fatSelected = false;
             sodiumSelected = false;
            priceSelected = false;
            keyword.setSelected(false);
            rating.setSelected(false);
            calories.setSelected(false);
            proteins.setSelected(false);
            fats.setSelected(false);
            sodium.setSelected(false);
            price.setSelected(false);
            keywordLabel.setVisible(false);
            searchKeyword.setVisible(false);
            searchKeyword.clear();
            ratingLabel.setVisible(false);
            searchRating.setVisible(false);
            searchRating.clear();
            caloriesLabel.setVisible(false);
            searchCalories.setVisible(false);
            searchCalories.clear();
            proteinLabel.setVisible(false);
            searchProtein.setVisible(false);
            searchProtein.clear();
            fatLabel.setVisible(false);
            searchFat.setVisible(false);
            searchFat.clear();
            sodiumLabel.setVisible(false);
            searchSodium.setVisible(false);
            searchSodium.clear();
            priceLabel.setVisible(false);
            searchPrice.setVisible(false);
            searchPrice.clear();

        }else if(e.getSource().equals(addProduct)){
            MenuItem item = menuTable.getSelectionModel().getSelectedItem();
            orderProducts.add(menuTable.getSelectionModel().getSelectedItem());
            for(MenuItem menuItem:deliveryService.getAllProducts()){
                if(menuItem.getTitle().equals(item.getTitle())){
                    menuItem.setTimesOrdered(menuItem.getTimesOrdered()+1);
                }
            }
        }else if(e.getSource().equals(placeOrder)){
            order.getProducts().addAll(orderProducts);
            int price = 0;
            for(MenuItem menuItem: order.getProducts()){
                price += menuItem.getPrice();
            }
            order.setTotalPrice(price);
            order.setClientId(client.getId());
            Random random = new Random();
            int orderId = random.nextInt(9000000-1)+1;
            Iterator iterator = deliveryService.getOrderInfo().entrySet().iterator();
            loop:
            while(iterator.hasNext()){
                Map.Entry element = (Map.Entry)iterator.next();
                if(((Order)element.getKey()).getOrderId() == orderId){
                    orderId = random.nextInt(9000000-1)+1;
                    continue loop;
                }
            }
            order.setOrderId(orderId);
            Date date = new Date();
            Date orderDate = new Date();
            orderDate.setHours(date.getHours());
            orderDate.setDate(date.getDate());
            order.setOrderDate(orderDate);
            for(Client clients : deliveryService.getClients()){
                if(client.getId() == clients.getId()){
                    clients.setTimesOrdered(clients.getTimesOrdered()+1);
                }
            }
            HashMap<Order,List<MenuItem>> newOrder;
            newOrder = deliveryService.createOrder(order);
            deliveryService.getOrderInfo().putAll(newOrder);
            FileWriter fileWriter = new FileWriter();
            fileWriter.makeBill(order,client);
            Serializator serializator = new Serializator();
            serializator.serializeAll(deliveryService);
            errorLabel.setText("The order was placed");
            errorLabel.setVisible(true);
        }
    }


}
