package businessLayer;

import javafx.scene.control.TableView;

import java.util.HashMap;
import java.util.List;

public interface IDeliveryServiceProcessing {

    public TableView<MenuItem> importProducts(TableView<MenuItem> table, int intializeTable, List<MenuItem> products);


    public void generateMinProducts(int min,HashMap<Order,List<MenuItem>> orders);

    public int generateOrderedInDay(int day, HashMap<Order,List<MenuItem>> orders);

    public int generateReportWithClients(int valueOfOrder, int nrOfTimesClientOrdered,List<Client> clients,HashMap<Order,List<MenuItem>> orders);

    /**
     * This method generates report based on the time interval of the orders
     * @pre min > 0
     */
    public int generateMinMaxHours(int min,int max,HashMap<Order,List<MenuItem>> orders);

    /**
     * This method modifies a product
     * @pre newProduct != null
     */
    public List<MenuItem> modifyProduct(MenuItem newProduct,MenuItem oldProduct,List<MenuItem> products);

    /**
     * This method deletes a product
     * @pre item != null
     */
    public List<MenuItem> deleteProduct(MenuItem item,List<MenuItem> products);

    /**
     * This method creates an order
     * @pre order != null
     */
    public HashMap<Order,List<MenuItem>> createOrder(Order order);
    /**
     * This method searches products by keyword
     * @pre keyword != null
     */
    public List<MenuItem> searchByKeyword(List<MenuItem> products,String keyword);

    /**
     * This method searches products by rating
     * @pre rating != null
     */
    public List<MenuItem> searchByRating(List<MenuItem> products,String rating);
    /**
     * This method searches products by calories
     * @pre calories != null
     */

    public List<MenuItem> searchByCalories(List<MenuItem> products,String calories);
    /**
     * This method searches products by proteins
     * @pre proteins != null
     */

    public List<MenuItem> searchByProteins(List<MenuItem> products,String proteins);
    /**
     * This method searches products by fat
     * @pre fat != null
     */

    public List<MenuItem> searchByFat(List<MenuItem> products,String fat);
    /**
     * This method searches products by sodium
     * @pre sodium != null
     */

    public List<MenuItem> searchBySodium(List<MenuItem> products,String sodium);

    /**
     * This method searches products by price
     * @pre price != null
     */
    public List<MenuItem> searchByPrice(List<MenuItem> products,String price);

    /**
     * This method adds a Base Product to the list of products
     * @pre baseProduct != null

     */
    public List<MenuItem> addBaseProduct(BaseProduct baseProduct,List<MenuItem> products);

    /**
     * This method adds a Composite Product to the list of products
     * @pre compositeProduct != null
     */

    public List<MenuItem> addCompositeProduct(CompositeProduct compositeProduct,List<MenuItem> products);

    /**
     * The initial products are imported
     * @pre true
     */
    public void readFromCsv();
}
