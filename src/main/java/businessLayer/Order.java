package businessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order implements Serializable {

    private static final long serialVersionUID = 123456L;

    private int orderId;
    private int clientId;
    private Date orderDate;
    private List<MenuItem> products;
    private int totalPrice;

    public Order(){
        products = new ArrayList<>();
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<MenuItem> getProducts() {
        return products;
    }

    public void setProducts(List<MenuItem> products) {
        this.products = products;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public int hashCode() {
        return clientId + orderId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this.clientId == ((Order)obj).clientId && this.orderId == ((Order)obj).getOrderId() && this.orderDate == ((Order)obj).getOrderDate()){
            return true;
        }
        return false;
    }
}
