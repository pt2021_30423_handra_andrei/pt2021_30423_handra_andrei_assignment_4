package businessLayer;
import dataLayer.FileWriter;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    private static final long serialVersionUID = 123456L;
    private HashMap<Order, List<MenuItem>> orderInfo;
    private List<MenuItem> allProducts;
    private List<Client> clients;

    public DeliveryService(){
        orderInfo = new HashMap<>();
        allProducts = new ArrayList<>();
        clients = new ArrayList<>();
    }
    public HashMap<Order, List<MenuItem>> getOrderInfo() {
        return orderInfo;
    }
    public void setOrderInfo(HashMap<Order, List<MenuItem>> orderInfo) {
        this.orderInfo = orderInfo;
    }
    public List<MenuItem> getAllProducts() {
        return allProducts;
    }
    public void setAllProducts(List<MenuItem> allProducts) {
        this.allProducts = allProducts;
    }
    public List<Client> getClients() {
        return clients;
    }
    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
    @Override
    public TableView<MenuItem> importProducts(TableView<MenuItem> table,int initiailzeTable,List<MenuItem> products) {
        TableView<MenuItem> menu;
        if(initiailzeTable == 0){
            table = setTableHeaders(table);
        }
        System.out.println(products.size());
        for(MenuItem menuItem:products){
            table.getItems().add(menuItem);
        }
        table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("title"));
        table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("rating"));
        table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("calories"));
        table.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("protein"));
        table.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("fat"));
        table.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("sodium"));
        table.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("price"));
        menu = table;
        return menu;
    }
    @Override
    public void generateMinProducts(int min, HashMap<Order, List<MenuItem>> orders) {
        Collection <List<MenuItem>> allOrders = orders.values();
        List<MenuItem> items = new ArrayList<>(),finalItems,tempItems = new ArrayList<>();
        for(List<MenuItem> list:allOrders){
            tempItems = list.stream().filter(p->p.getTimesOrdered() > min).collect(Collectors.toList());
            items.addAll(tempItems);
        }
        if(items.size() != 0) {
            finalItems = items.stream().filter(distinctTitle(p -> p.getTitle())).collect(Collectors.toList());
            FileWriter fileWriter = new FileWriter();
            fileWriter.makeMinProductsReport(finalItems,min);
        }
    }
    @Override
    public int generateOrderedInDay(int day, HashMap<Order, List<MenuItem>> orders) {
        Set<Order> allOrders = orders.keySet();
        List<Order> goodOrders = allOrders.stream().filter(p->p.getOrderDate().getDay() == day).collect(Collectors.toList());
        if(goodOrders.size() == 0){ return 0; }
        List<MenuItem> tempList = new ArrayList<>();
        for(Order order:goodOrders){
            tempList.addAll(order.getProducts());
        }
        Collections.sort(tempList);
        FileWriter fileWriter = new FileWriter();
        fileWriter.makeInDayReport(tempList,day);
        return goodOrders.size();
    }
    @Override
    public int generateReportWithClients(int valueOfOrder, int nrOfTimesClientOrdered, List<Client> clients, HashMap<Order, List<MenuItem>> orders) {
        Set<Order> allOrders = orders.keySet();
        List<Order> orderList = allOrders.stream().collect(Collectors.toList());
        List<Client> goodClients = new ArrayList<>();
        for(Client client: clients){
            if(client.getTimesOrdered() > nrOfTimesClientOrdered){
                for(Order order:orderList){
                    if(order.getClientId() == client.getId()){
                        if(order.getTotalPrice() > valueOfOrder){
                            goodClients.add(client); } } } }
        }
        if(goodClients.size() == 0){ return 0; }
        List<Client> finalList = goodClients.stream().filter(distinctId(p->p.getId())).collect(Collectors.toList());
        FileWriter fileWriter = new FileWriter();
        fileWriter.makeClientsReport(finalList,valueOfOrder,nrOfTimesClientOrdered);
        return goodClients.size();
    }
    @Override
    public int generateMinMaxHours(int min, int max, HashMap<Order, List<MenuItem>> orders) {
        assert min > 0;
        Set<Order> allOrders = orders.keySet();
        List<Order> goodOrders = allOrders.stream().filter(p->p.getOrderDate().getHours() > min && p.getOrderDate().getHours() < max).collect(Collectors.toList());
        if(goodOrders.size() == 0){ return 0; }
        FileWriter fileWriter = new FileWriter();
        fileWriter.makeMinMaxReport(goodOrders,min,max);
        return goodOrders.size();
    }
    @Override
    public List<MenuItem> modifyProduct(MenuItem newProduct,MenuItem oldProduct,List<MenuItem> products) {
        assert newProduct!=null;
        List<MenuItem> newList;
        for(int i=0;i<products.size();i++){
            if(products.get(i).getTitle().equals(oldProduct.getTitle())){
                products.get(i).setTitle(newProduct.getTitle());
                products.get(i).setRating(newProduct.getRating());
                products.get(i).setCalories(newProduct.getCalories());
                products.get(i).setProtein(newProduct.getProtein());
                products.get(i).setFat(newProduct.getFat());
                products.get(i).setSodium(newProduct.getSodium());
                products.get(i).computePrice(newProduct.getPrice()); }
        }
        newList = products;
        return newList;
    }
    @Override
    public List<MenuItem> deleteProduct(MenuItem item,List<MenuItem> products) {
        assert item != null;
        List<MenuItem> newList;
        products.removeIf(p->p.getTitle().equals(item.getTitle()));
        System.out.println(products.size());
        newList = products;
        return newList;
    }
    @Override
    public HashMap<Order,List<MenuItem>> createOrder(Order order) {
        assert order != null;
        HashMap<Order,List<MenuItem>> orderInfo = new HashMap<>();
        orderInfo.put(order,order.getProducts());
        setChanged();
        notifyObservers(order);
        return orderInfo;
    }
    @Override
    public List<MenuItem> searchByKeyword(List<MenuItem> products, String keyword) {
        assert keyword != null;
        List<MenuItem> list;
        list = products.stream().filter(p->p.getTitle().contains(keyword)).collect(Collectors.toList());
        return list;
    }
    @Override
    public List<MenuItem> searchByRating(List<MenuItem> products, String rating) {
        assert rating != null;
        List<MenuItem> list = null;
        if(rating.charAt(0) == '<'){
            list = products.stream().filter(p->p.getRating() < Double.parseDouble(rating.substring(1))).collect(Collectors.toList());
        }else if(rating.charAt(0) == '='){
            list = products.stream().filter(p->p.getRating() == Double.parseDouble(rating.substring(1))).collect(Collectors.toList());
        }else if(rating.charAt(0) == '>'){
            list = products.stream().filter(p->p.getRating() > Double.parseDouble(rating.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> searchByCalories(List<MenuItem> products, String calories) {
        assert calories != null;
        List<MenuItem> list = null;
        if(calories.charAt(0) == '<'){
            list = products.stream().filter(p->p.getCalories() < Integer.parseInt(calories.substring(1))).collect(Collectors.toList());
        }else if(calories.charAt(0) == '='){
            list = products.stream().filter(p->p.getCalories() == Integer.parseInt(calories.substring(1))).collect(Collectors.toList());
        }else if(calories.charAt(0) == '>'){
            list = products.stream().filter(p->p.getCalories() > Integer.parseInt(calories.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> searchByProteins(List<MenuItem> products, String proteins) {
        assert proteins != null;
        List<MenuItem> list = null;
        if(proteins.charAt(0) == '<'){
            list = products.stream().filter(p->p.getProtein() < Integer.parseInt(proteins.substring(1))).collect(Collectors.toList());
        }else if(proteins.charAt(0) == '='){
            list = products.stream().filter(p->p.getProtein() == Integer.parseInt(proteins.substring(1))).collect(Collectors.toList());

        }else if(proteins.charAt(0) == '>'){
            list = products.stream().filter(p->p.getProtein() > Integer.parseInt(proteins.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> searchByFat(List<MenuItem> products, String fat) {
        assert fat != null;
        List<MenuItem> list = null;
        if(fat.charAt(0) == '<'){
            list = products.stream().filter(p->p.getFat() < Integer.parseInt(fat.substring(1))).collect(Collectors.toList());
        }else if(fat.charAt(0) == '='){
            list = products.stream().filter(p->p.getFat() == Integer.parseInt(fat.substring(1))).collect(Collectors.toList());

        }else if(fat.charAt(0) == '>'){
            list = products.stream().filter(p->p.getFat() > Integer.parseInt(fat.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> searchBySodium(List<MenuItem> products, String sodium) {
        assert sodium != null;
        List<MenuItem> list = null;
        if(sodium.charAt(0) == '<'){
            list = products.stream().filter(p->p.getSodium() < Integer.parseInt(sodium.substring(1))).collect(Collectors.toList());
        }else if(sodium.charAt(0) == '='){
            list = products.stream().filter(p->p.getSodium() == Integer.parseInt(sodium.substring(1))).collect(Collectors.toList());

        }else if(sodium.charAt(0) == '>'){
            list = products.stream().filter(p->p.getSodium() > Integer.parseInt(sodium.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> searchByPrice(List<MenuItem> products, String price) {
        assert price != null;
        List<MenuItem> list = null;
        if(price.charAt(0) == '<'){
            list = products.stream().filter(p->p.getPrice() < Integer.parseInt(price.substring(1))).collect(Collectors.toList());
        }else if(price.charAt(0) == '='){
            list = products.stream().filter(p->p.getPrice() == Integer.parseInt(price.substring(1))).collect(Collectors.toList());
        }else if(price.charAt(0) == '>'){
            list = products.stream().filter(p->p.getPrice() > Integer.parseInt(price.substring(1))).collect(Collectors.toList());
        }
        return list;
    }
    @Override
    public List<MenuItem> addBaseProduct(BaseProduct baseProduct,List<MenuItem> products) {
        assert baseProduct!=null;
        List<MenuItem> newList;
        products.add(baseProduct);
        newList = products;
        return newList;
    }
    @Override
    public List<MenuItem> addCompositeProduct(CompositeProduct compositeProduct,List<MenuItem> products) {
        assert compositeProduct != null;
        List<MenuItem> newList;
        products.add(compositeProduct);
        newList = products;
        return newList;
    }
    @Override
    public void readFromCsv(){
        try {
            BufferedReader csvProducts = new BufferedReader(new FileReader("products.csv"));
            List<MenuItem> allProducts2 = new ArrayList<>();
            csvProducts.readLine();
            String row = csvProducts.readLine();
            while(row != null){
                BaseProduct product = new BaseProduct();
                String[] info = row.split(",");
                product.setTitle(info[0]);
                product.setRating(Double.parseDouble(info[1]));
                product.setCalories(Integer.parseInt(info[2]));
                product.setProtein(Integer.parseInt(info[3]));
                product.setFat(Integer.parseInt(info[4]));
                product.setSodium(Integer.parseInt(info[5]));
                product.computePrice(Integer.parseInt(info[6]));
                product.setTimesOrdered(0);
                allProducts2.add(product);
                row = csvProducts.readLine();
            }
            allProducts = allProducts2.stream().filter(distinctTitle(p->p.getTitle())).collect(Collectors.toList());
        }catch(FileNotFoundException e){ e.printStackTrace();
        }catch(IOException e){ e.printStackTrace(); }
    }
    public static Predicate<MenuItem> distinctTitle(Function<MenuItem, String> keyExtractor){
        Set<Object> first = ConcurrentHashMap.newKeySet();
        return t -> first.add(keyExtractor.apply(t));
    }
    public static Predicate<Client> distinctId(Function<Client, Integer> keyExtractor){
        Set<Object> first = ConcurrentHashMap.newKeySet();
        return t -> first.add(keyExtractor.apply(t));
    }
    public TableView<MenuItem> setTableHeaders(TableView<MenuItem> table){
        TableView<MenuItem> menu;
        List<String> headers = List.of("Title","Rating","Calories","Protein","Fat","Sodium","Price");
        for(String string:headers){
            TableColumn<MenuItem,String> tableColumn = new TableColumn<>(string);
            table.getColumns().add(tableColumn); }
        menu = table;
        return menu;
    }
}