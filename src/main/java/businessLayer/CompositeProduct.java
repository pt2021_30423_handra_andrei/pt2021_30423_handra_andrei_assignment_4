package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem{

    List<MenuItem> menuItems;

    public CompositeProduct(){
        menuItems = new ArrayList<>();
    }

    public List<MenuItem> getCompositeProducts() {
        return menuItems;
    }

    public void setCompositeProducts(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public void computePrice(){
        for(MenuItem menuItem:menuItems){
            this.computePrice(this.getPrice()+menuItem.getPrice());
        }
    }
}
