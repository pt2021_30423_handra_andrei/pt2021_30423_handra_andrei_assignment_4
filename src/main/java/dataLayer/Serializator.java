package dataLayer;

import businessLayer.DeliveryService;

import java.io.*;
import java.util.ArrayList;

public class Serializator{


    public void serializeAll(DeliveryService deliveryService){
        try {
            FileOutputStream file = new FileOutputStream("serialize.txt");
            ObjectOutputStream object = new ObjectOutputStream(file);
            object.writeObject(deliveryService);
            object.close();
            file.close();
        }catch (FileNotFoundException e){
            System.out.println("File was not found");
        }catch (IOException e){
            System.out.println("Input/Output exception");
        }
    }
    public DeliveryService deserializeAll(){
        DeliveryService deliveryService = new DeliveryService();
        try{
            FileInputStream file = new FileInputStream("serialize.txt");
            ObjectInputStream object = new ObjectInputStream(file);
            deliveryService = (DeliveryService)object.readObject();
            object.close();
            file.close();
        }catch(FileNotFoundException e) {
            System.out.println("a");
        }catch(IOException e){
            System.out.println("nothing to deserialize");
        }catch(ClassNotFoundException e){
            System.out.println("Class not found exception");
        }
        return deliveryService;
    }
}
