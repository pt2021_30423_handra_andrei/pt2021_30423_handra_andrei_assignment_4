package dataLayer;

import businessLayer.Client;
import businessLayer.MenuItem;
import businessLayer.Order;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

public class FileWriter {

    private Font font1 = FontFactory.getFont(FontFactory.COURIER_BOLD,20, BaseColor.BLACK);
    private Font font2 = FontFactory.getFont(FontFactory.COURIER,15,BaseColor.BLACK);
    private Paragraph paragraph;
    private Paragraph emptyParagraph = new Paragraph(" ");

    public void prepareParagraph(int allignment,Font font){
        paragraph = new Paragraph();
        paragraph.setAlignment(allignment);
        paragraph.setFont(font);
    }

    public void makeMinMaxReport(List<Order> orders,int min,int max){
        Document document = new Document();
        try{
            PdfWriter.getInstance(document,new FileOutputStream("MinMaxReport.pdf"));
            document.open();
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("The orders made between the hours "+min+" and "+ max + " are:");
            document.add(paragraph);
            document.add(emptyParagraph);
            for(Order order: orders){
                prepareParagraph(Paragraph.ALIGN_LEFT,font2);
                paragraph.add(" Order Id " +order.getOrderId()+" Client id " + order.getClientId());
                paragraph.add(" with products: ");
                for(MenuItem menuItem:order.getProducts()){
                    paragraph.add(" " + menuItem.getTitle() + " , ");
                }
                document.add(paragraph);
            }
            document.close();
        }catch (FileNotFoundException e) { e.printStackTrace();
        }catch (DocumentException e){ e.printStackTrace();}
    }
    public void makeMinProductsReport(List<MenuItem> list,int min){
        Document document = new Document();
        try{
            PdfWriter.getInstance(document,new FileOutputStream("MinProductsReport.pdf"));
            document.open();
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("The products ordered more than "+min+" times are: ");
            document.add(paragraph);
            document.add(emptyParagraph);
            for(MenuItem menuItem: list){
                prepareParagraph(Paragraph.ALIGN_LEFT,font2);
                paragraph.add(menuItem.getTitle()+", ");
            }
            document.add(paragraph);
            document.close();
        }catch (FileNotFoundException e) { e.printStackTrace();
        }catch (DocumentException e){ e.printStackTrace();}
    }
    public void makeInDayReport(List<MenuItem> list, int day){
        Document document = new Document();
        try{
            PdfWriter.getInstance(document,new FileOutputStream("InDayReport.pdf"));
            document.open();
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("The products ordered on the day "+day+" times are: ");
            document.add(paragraph);
            document.add(emptyParagraph);
            for(int i=0;i<list.size();i++){
                MenuItem item = list.get(i);
                int nrRepetitions = 1;
                for(int j=i+1;j<list.size() && item.getTitle().equals(list.get(j).getTitle());j++){
                    nrRepetitions++;
                }
                prepareParagraph(Paragraph.ALIGN_LEFT,font2);
                paragraph.add(item.getTitle()+", "+"ordered "+nrRepetitions+ " times");
                i = i+nrRepetitions-1;
                document.add(paragraph);
            }
            document.close();
        }catch (FileNotFoundException e) { e.printStackTrace();
        }catch (DocumentException e){ e.printStackTrace();}
    }

    public void makeClientsReport(List<Client> list,int valueOfOrder, int nrOfTimesClientOrdered){
        Document document = new Document();
        try{
            PdfWriter.getInstance(document,new FileOutputStream("ClientsReport.pdf"));
            document.open();
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("The clients who ordered more than "+nrOfTimesClientOrdered+" times with the order value more than: "+valueOfOrder+" are: ");
            document.add(paragraph);
            document.add(emptyParagraph);
            for(Client client:list){
                prepareParagraph(Paragraph.ALIGN_LEFT,font2);
                paragraph.add("Client " + client.getUsername() + " with client id: "+client.getId());
                document.add(paragraph);
            }
            document.close();
        }catch (FileNotFoundException e) { e.printStackTrace();
        }catch (DocumentException e){ e.printStackTrace();}
    }

    public void makeBill(Order order, Client client){
        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("Order" + order.getOrderId() + ".pdf"));
            document.open();
            prepareParagraph(Paragraph.ALIGN_CENTER,font1);
            paragraph.add("Order confirmed!");
            document.add(paragraph);
            document.add(emptyParagraph);
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("Thank you "+client.getUsername()+ " for your order");
            document.add(paragraph);
            document.add(emptyParagraph);
            prepareParagraph(Paragraph.ALIGN_LEFT,font2);
            paragraph.add("The ordered products are: ");
            document.add(paragraph);
            document.add(emptyParagraph);
            PdfPTable table = new PdfPTable(2);
            addTableHeader(table);
            for(MenuItem menuItem:order.getProducts()){
                addRows(table,menuItem);
            }
            document.add(table);
            document.add(emptyParagraph);
            prepareParagraph(Paragraph.ALIGN_RIGHT,font2);
            paragraph.add("Total price: "+order.getTotalPrice() + " lei");
            document.add(paragraph);
            document.add(emptyParagraph);
            prepareParagraph(Paragraph.ALIGN_RIGHT,font2);
            paragraph.add("Current date: " + LocalDate.now());
            document.add(paragraph);
            document.close();
        }catch (DocumentException e){ System.out.println("Document exception");
        }catch (FileNotFoundException e){ System.out.println("File was not found"); }
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Product","Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table,MenuItem menuItem) {
        table.addCell(menuItem.getTitle());
        table.addCell(String.valueOf(menuItem.getPrice()));
    }
}
